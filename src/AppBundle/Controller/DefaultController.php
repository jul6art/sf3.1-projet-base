<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/")
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(){
        if ($this->getUser()){
            $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
        } else {
            $users = [];
        }
        return $this->render('default/index.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/user/edit/{id}", name="edit_user")
     * @Method({"GET", "POST"})
     * @param Request $request
     */
    public function editUserAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(User::class);
        $entity = $repository->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }
        $editForm = $this->createForm(UserType::class,$entity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();
            // return valid response

            $this->get('session')->getFlashBag()->add(
                'success','entity.edit.success'
            );
            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render('default/user_edit.html.twig', [
            'form' => $editForm->createView()
        ]);

    }

}
