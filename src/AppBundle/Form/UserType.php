<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $choices = \AppBundle\Constants\Constants::getInstance()->getRoles();

        $builder
            ->add('username', null, ['label' => 'user.username'])
            ->add('email', EmailType::class, ['label' => 'user.email'])
            ->add('enabled', CheckboxType::class, ['label' => 'user.enabled', 'required' => false])
            ->add('locked', CheckboxType::class, ['label' => 'user.locked', 'required' => false])
            ->add('confirmationToken', null, ['label' => 'user.confirmation_token', 'required' => false])
            ->add('roles', ChoiceType::class, ['label' => 'user.roles', 'required' => true, 'multiple' => true, 'choices' => $choices])
            ->add('ok', SubmitType::class, ['label' => 'actions.submit'])
        ;


    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
