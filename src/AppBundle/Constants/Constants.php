<?php

namespace AppBundle\Constants;

/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 21-11-16
 * Time: 11:35
 */
class Constants
{
    private static $_instance = null;

    const ROLE_USER = 'ROLE_USER';
    const ROLE_VISITOR = 'ROLE_VISITOR';
    const ROLE_ADMIN = 'ROLE_ADMIN';

    public static function getInstance() {
        if(is_null(self::$_instance)) {
            self::$_instance = new Constants();
        }
        return self::$_instance;
    }

    private function __construct() {
        $this->roles = array(
            'Admin' => self::ROLE_ADMIN,
            'User' => self::ROLE_USER,
            'Visitor' => self::ROLE_VISITOR, )
        ;
    }


    public function getRoles(){
        return $this->roles;
    }
}